---
layout: handbook-page-toc
title: Home Page for Support's Maple Group
description: Home Page for Support's Maple Group
---

<!-- Search for all occurences of NAME and replace them with the group's name.
     Search for all occurences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of the Maple group

Introductory text, logos, or whatever the group wants here

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Maple resources

- Our Slack Channel: [spt_gg_maple](https://gitlab.slack.com/archives/C03C9EHQ97V)
- Our Team: [Maple Members](https://gitlab-com.gitlab.io/support/team/sgg.html?search=maple)

## Maple workflows and processes

